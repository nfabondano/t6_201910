package controller;

import java.io.BufferedReader;
import java.io.File;
import model.vo.LocationVO;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;

import com.opencsv.CSVReader;

import jdk.nashorn.api.scripting.JSObject;

import org.json.*;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;

import model.data_structures.HashTableLP;
import model.data_structures.HashTableSC;
import model.data_structures.IOrderedResizingArrayList;
import model.data_structures.ITablaHash;
import model.data_structures.OrderedResizingArrayList;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	/**
	 * Tabla donde se van a guardar los datos en forma de SC
	 */
	private ITablaHash<Double, OrderedResizingArrayList<VOMovingViolations>> tablaSC;


	/**
	 * Tabla donde se van a guardar los datos en forma de LP
	 */
	private ITablaHash<Double, OrderedResizingArrayList<VOMovingViolations>> tablaLP;

	/**
	 * Constante del archivo de enero.
	 */
	public final static String ENERO = "./data/Moving_Violations_Issued_in_January_2018.json";

	/**
	 * Constante del archivo de febrero.
	 */
	public final static String FEBRERO = "./data/Moving_Violations_Issued_in_February_2018.json";

	/**
	 * Constante del archivo de marzo.
	 */
	public final static String MARZO = "./data/Moving_Violations_Issued_in_March_2018.json";

	/**
	 * Constante del archivo de abril.
	 */
	public final static String ABRIL = "./data/Moving_Violations_Issued_in_April_2018.json";

	/**
	 * Constante del archivo de mayo.
	 */
	public final static String MAYO = "./data/Moving_Violations_Issued_in_May_2018.json";

	/**
	 * Constante del archivo de junio.
	 */
	public final static String JUNIO = "./data/Moving_Violations_Issued_in_June_2018.json";

	/**
	 * forma de conectarse con la clase que imprime informaci�n a la consola
	 */
	private MovingViolationsManagerView view;

	/**
	 * Lista donde se van a cargar los datos de los archivos
	 */
	private IOrderedResizingArrayList<VOMovingViolations> movingViolationsArrayOrdered;	

	/**
	 * Archivos para leer en JSON.
	 */
	private String[] jsonData;

	/**
	 * Comparador por fecha.
	 */
	private Comparator<VOMovingViolations> comparadorFecha;

	/**
	 * Comparador por objectId.
	 */
	private Comparator<VOMovingViolations> comparadorObject;

	/**
	 * Comparador por violation code.
	 */
	private Comparator<VOMovingViolations> comparadorCode;

	/**
	 * Comparador por hora.
	 */
	private Comparator<VOMovingViolations> comparadorHora;

	/**
	 * Comparador por violation description.
	 */
	private Comparator<VOMovingViolations> comparadorDesc;

	/**
	 * Comparador por streetSegidIdInvertido
	 */
	private Comparator<VOMovingViolations> comparadorStreetSegId;

	/**
	 * comparador por streetID
	 */
	private Comparator<VOMovingViolations> comparadorStreetId;

	/**
	 * Tama�o del arreglo
	 */
	private int size;

	/**
	 * Constructor del controlador
	 */
	public Controller() {
		view = new MovingViolationsManagerView();
		jsonData = new String[6];
		jsonData[0] = ENERO; 
		jsonData[1] = FEBRERO;
		jsonData[2] = MARZO;
		jsonData[3] = ABRIL;
		jsonData[4] = MAYO;
		jsonData[5] = JUNIO;
		comparadorFecha = new VOMovingViolations.ComparatorDate();
		comparadorObject = new VOMovingViolations.ComparatorObjectID();
		comparadorCode = new VOMovingViolations.ComparatorViolationCode();
		comparadorDesc = new VOMovingViolations.ComparatorViolationDesc();
		comparadorHora = new VOMovingViolations.ComparatorHour();
		comparadorStreetSegId = new VOMovingViolations.ComparadorStreetSegId();
		comparadorStreetId = new VOMovingViolations.ComparadorStreetId();
		size = 55001;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				loadMovingViolations();
				break;
			case 1:
				view.printMessage("Ingrese el ADRESS_ID, por ejemplo: 277954");
				double adressID1 =  sc.nextDouble();
				view.printTabla(tablaLP.get(adressID1));
				break;
			case 2:	
				view.printMessage("Ingrese el ADRESS_ID, por ejemplo: 277954");
				double adressID2 =  sc.nextDouble();
				view.printTabla(tablaSC.get(adressID2));
				break;
			case 3:	
				fin=true;
				sc.close();
				break;
			}
		}

	}

	/**
	 * M�todo que carga las infracciones
	 */
	public void loadMovingViolations() {
		tablaSC = new HashTableSC<Double,OrderedResizingArrayList<VOMovingViolations>>(size);
		tablaLP = new HashTableLP<Double, OrderedResizingArrayList<VOMovingViolations>>(size);
		OrderedResizingArrayList<VOMovingViolations> lista = new OrderedResizingArrayList<VOMovingViolations>(10);
		for(int i = 0; i<6; i++) {
			try {
				FileReader nm = new FileReader(jsonData[i]);
				JSONParser parser = new JSONParser();
				JSONArray a = (JSONArray) parser.parse(nm);
				for (Object o : a) {
					JSONObject obj = (JSONObject) o;
					if(obj.get("ADDRESS_ID") != null) {
						VOMovingViolations vo = new VOMovingViolations(obj);
						if(tablaLP.contains(vo.getAddressID())) {
							lista = tablaLP.get(vo.getAddressID());
							lista.add(vo);
							tablaLP.put(vo.getAddressID(), lista);
							tablaSC.put(vo.getAddressID(), lista);
						}
						else {
							lista = new OrderedResizingArrayList<VOMovingViolations>(10);
							lista.add(vo);
							tablaLP.put(vo.getAddressID(), lista);
							tablaSC.put(vo.getAddressID(), lista);
						}
					}
				}

				Iterator<Double> it = tablaLP.keys();
				while(it.hasNext()) {
					Double llave = it.next();
					OrderedResizingArrayList<VOMovingViolations> infrac = tablaSC.get(llave);
					infrac.sort(comparadorFecha);
					tablaSC.put(llave, infrac);
					tablaLP.put(llave, infrac);
				}
			}
			catch(Exception ex) {
				System.out.println("Unable to charge file '" + jsonData[i] + "'");
			}
		}
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora) {
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
}
