package view;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.ITablaHash;
import model.data_structures.OrderedResizingArrayList;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("0. Cargar datos del primer semestre en tabla hash por ADRESSID");
		System.out.println("1. Genera TablaHash(Linear Probing) por ADRESSID ordenada cronologicamente");
		System.out.println("2. Genera TablaHash(Separate Chaining) por ADRESSID ordenada cronologicamente");
		System.out.println("3. Salir");
		System.out.println("Digite el nï¿½mero de opciï¿½n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}
	
	public void printTabla(OrderedResizingArrayList<VOMovingViolations> lista) {
		System.out.println("OBJECTID\t LOCATION\t TICKETISSUEDATE\t VIOLATIONCODE\t FINEAMT\t");

		for(VOMovingViolations v: lista) {
			System.out.println( v.getObjectID() + "\t" + v.getLocation() + "\t" + v.getTicketIssueDate() + "\t" + v.getViolationCode() + "\t" + v.getfineAMT());
		}
	}
}